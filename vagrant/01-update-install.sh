#!/bin/bash

echo '-------------------------------------------------'
echo '------ install updates and basic software -------'
echo '-------------------------------------------------'

# system upgrades
apt-get update
apt-get dist-upgrade -y

# base packages
apt-get -y install vim
apt-get -y install htop

#Set timezone
timedatectl set-timezone Europe/Berlin
