#!/bin/bash

echo '-------------------------------------------------'
echo '--------- apache is getting configured ----------'
echo '-------------------------------------------------'

#apache2, mysql & php
apt-get -y install apache2 php php-xdebug

apt-get -y install libapache2-mod-php7.0

rm -r /var/www/html
ln -s /vagrant /var/www/html

sudo service apache2 restart	
