<?php
/**
 * Einstiegstext → Probe → Gelungen/Misslungen Text → Wenn Gelungen weitere Probe
 */

if($aoqml != true) {
  ?>

  <fieldset>
    <legend>Einstiegstext</legend>
    <?= input_text('start'); ?>
  </fieldset>

  <fieldset>
    <legend>Erste Probe</legend>
    <?= input_challenge('start'); ?>
  </fieldset>

  <fieldset>
    <legend>Zweite Probe (Erste war erfolgreich)</legend>
    <?= input_challenge('win'); ?>
  </fieldset>


  <?php
}
else{

  // generate AOQML
  include('includes/snippets/aoqml-header.php');

  echo aoqml_text($_POST['start']);
  echo aoqml_challenge('start', 'win');

  include('includes/snippets/aoqml-footer.php');
}