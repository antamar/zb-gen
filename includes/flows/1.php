<?php
/**
 * Einstiegstext → Probe → Gelungen/Misslungen Text → Gelungen/Misslungen Belohnung.
 */

if($aoqml != true) {
    ?>

    <fieldset>
        <legend>Einstiegstext</legend>
        <?= input_text('start'); ?>
    </fieldset>

    <fieldset>
        <legend>Probe</legend>
        <?= input_challenge('start'); ?>
    </fieldset> <?php
}
else{

    // generate AOQML
    include('includes/snippets/aoqml-header.php');

    echo aoqml_text($_POST['start']);
    echo aoqml_challenge('start', 'simple');

    include('includes/snippets/aoqml-footer.php');
}