<?php

if(isset($_POST['autor']) && strlen($_POST['autor']) > 1){
  $autor = $_POST['autor'];
}
else{
  $autor = '';
}

if(isset($_POST['zb_name']) && strlen($_POST['zb_name']) > 2){
  $zb_name = $autor. '_'. $_POST['zb_name'];
}
else{
  $zb_name = $autor. '_'. substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzQWERTZUIOPASDFGHJKLYXCVBNM"), 0, 5);
}


?>

<script>function copy() {
        let textarea = document.getElementById("aoqmltext");
        textarea.select();
        document.execCommand("copy");
    }</script>


<p>Du kannst noch letzte Anpassungen vornehmen und dann den fertigen Text kopieren.</p>

<p><button onclick="copy()">Text in Zwischenablage kopieren</button></p>

<p><a href="https://wiki.antamar.eu/index.php?title=ZB_Editor_<?=$zb_name?>&action=edit" onclick="copy()">Hier ist der generierte Link für das Wiki. Füge den Text einfach dort ein und speichere ab.</a> (Der Text wird beim Klick auch noch einmal in die Zwischenablage kopiert, keine Sorge)<p>
<p>Die ZB wird korrekturgelesen und dann in das Spiel eingebaut. Danke für deine Mithilfe :)</p>

<?php
echo '<textarea id="aoqmltext" class="notiny" cols="200" rows="45">';
echo htmlspecialchars('{{spoiler}}') . "\n";
echo "\n";
echo htmlspecialchars('{{Vorlage:Infobox ZB') . "\n";
echo htmlspecialchars('| autor='.$autor) . "\n";
echo htmlspecialchars('  | gruppe=nein') . "\n";
echo htmlspecialchars('  | gegend=<!--wo stößt man auf die ZB?-->') . "\n";
echo htmlspecialchars('  | häufigkeit=<!--wie Häufig ist die ZB-->') . "\n";
echo htmlspecialchars('  | humor=<!--Humor, ja oder nein-->') . "\n";
echo htmlspecialchars('  | status=Abnahme') . "\n";
echo htmlspecialchars('  | aoqml=ja') . "\n";
echo htmlspecialchars('}}') . "\n";
echo "\n";
echo "\n";
echo htmlspecialchars('==Beschreibung==') . "\n";
echo "\n";
echo htmlspecialchars('<!-- hier Beschreibung ohne AOQML -->') . "\n";
echo "\n";
echo "\n";
echo htmlspecialchars('==AOQML==') . "\n";
echo "\n";
echo htmlspecialchars('<syntaxhighlight lang="xml">') . "\n";
echo "\n";
echo "\n";
echo htmlspecialchars('<?xml version="1.0" encoding="UTF-8"?>') . "\n";
echo htmlspecialchars('<scene xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://eisentrutz.antamar.eu/aoqml.xsd">') . "\n";
echo "\n";
echo htmlspecialchars('<!-- Humor=Ja/Nein; Gegend= bitte ausfuellen; Häufigkeit=selten; GZB=nein -->') . "\n";
echo htmlspecialchars('<!-- Autor: '.$autor.'; Titel: '.$zb_name.' -->') . "\n";