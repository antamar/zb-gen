<?php

/**
 * shows textarea with given name
 * @param $text_name
 */
function input_text($text_name){

    echo '<textarea name="'.$text_name.'"></textarea>';

}

/**
 * shows HTML-text
 * @param $text
 */
function aoqml_text($text){
  $content = "\n" . htmlspecialchars($text) . "\n";

  return $content;
}