<?php

/**
 * @param $position start, win - Zweig bei Verschachtelung, loss - Zweig bei Verschachtelung
 */
function input_challenge($position){

    // Fertigkeiten Liste: @todo: aus xsd generieren?
    $talent_select = '    <select name="talent_'.$position.'" size="1">
                            <optgroup label="Fernwaffen">
                              <option label="Armbrüste">Armbrüste</option>
                              <option label="Blasrohre">Blasrohre</option>
                              <option label="Bögen">Bögen</option>
                              <option label="Chakrams">Chakrams</option>
                              <option label="Schleudern">Schleudern</option>
                              <option label="Wurfbeile">Wurfbeile</option>
                              <option label="Wurfspeere">Wurfspeere</option>
                              <option label="Schusswaffen">Schusswaffen</option>
                              <option label="Schiffsgeschütze">Schiffsgeschütze</option>
                              <option label="Wurfmesser">Wurfmesser</option>
                              <option label="Katapulte/Ballisten">Katapulte/Ballisten</option>
                            <optgroup label="Gesellschaft">
                              <option label="Seelenheilung">Seelenheilung</option>
                              <option label="Menschenkenntnis">Menschenkenntnis</option>
                              <option label="Überreden">Überreden</option>
                              <option label="Einschüchtern">Einschüchtern</option>
                              <option label="Schauspielerei">Schauspielerei</option>
                              <option label="Schriftstellerei">Schriftstellerei</option>
                              <option label="Verführen">Verführen</option>
                              <option label="Etikette">Etikette</option>
                              <option label="Gassenwissen">Gassenwissen</option>
                              <option label="Lehren">Lehren</option>
                              <option label="Verkleiden">Verkleiden</option>
                              <option label="Überzeugen">Überzeugen</option>
                              <option label="Falschspiel">Falschspiel</option>
                              <option label="Diplomatie">Diplomatie</option>
                              <option label="Lippen lesen">Lippen lesen</option>
                              <option label="Feilschen">Feilschen</option>
                              <option label="Rhetorik">Rhetorik</option>
                              <option label="Galanterie">Galanterie</option>
                              <option label="Dichtkunst">Dichtkunst</option>
                              <option label="Verbergen">Verbergen</option>
                              <option label="Verhören">Verhören</option>
                            <optgroup label="Handwerk/Berufsfertigkeiten">
                              <option label="Bäcker">Bäcker</option>
                              <option label="Schusswaffenbau">Schusswaffenbau</option>
                              <option label="Fälschen">Fälschen</option>
                              <option label="Armbruster">Armbruster</option>
                              <option label="Fallen entschärfen">Fallen entschärfen</option>
                              <option label="Erste Hilfe">Erste Hilfe</option>
                              <option label="Stehlen">Stehlen</option>
                              <option label="Wundenheilung">Wundenheilung</option>
                              <option label="Holzbearbeitung">Holzbearbeitung</option>
                              <option label="Kochen">Kochen</option>
                              <option label="Lederarbeiten">Lederarbeiten</option>
                              <option label="Zeichnen">Zeichnen</option>
                              <option label="Schneidern">Schneidern</option>
                              <option label="Landwirtschaft">Landwirtschaft</option>
                              <option label="Alchimie">Alchimie</option>
                              <option label="Bergbau">Bergbau</option>
                              <option label="Bogenbau">Bogenbau</option>
                              <option label="Brauen">Brauen</option>
                              <option label="Drucker">Drucker</option>
                              <option label="Feinmechanik">Feinmechanik</option>
                              <option label="Fleischer">Fleischer</option>
                              <option label="Gerben/Kürschnern">Gerben/Kürschnern</option>
                              <option label="Glaskunst">Glaskunst</option>
                              <option label="Handel">Handel</option>
                              <option label="Hauswirtschaft">Hauswirtschaft</option>
                              <option label="Instrumentenbau">Instrumentenbau</option>
                              <option label="Kristallzucht">Kristallzucht</option>
                              <option label="Maurer">Maurer</option>
                              <option label="Metallguss">Metallguss</option>
                              <option label="Alkohol brennen">Alkohol brennen</option>
                              <option label="Seefahrt">Seefahrt</option>
                              <option label="Seiler">Seiler</option>
                              <option label="Steinmetz">Steinmetz</option>
                              <option label="Steinschneider">Steinschneider</option>
                              <option label="Stellmacher">Stellmacher</option>
                              <option label="Viehzucht">Viehzucht</option>
                              <option label="Winzer">Winzer</option>
                              <option label="Zimmermann">Zimmermann</option>
                              <option label="Boote fahren">Boote fahren</option>
                              <option label="Stoffe Färben">Stoffe Färben</option>
                              <option label="Fahrzeug lenken">Fahrzeug lenken</option>
                              <option label="Feuersteinbearbeitung">Feuersteinbearbeitung</option>
                              <option label="Schmiedekunst">Schmiedekunst</option>
                              <option label="Giftheilung">Giftheilung</option>
                              <option label="Krankheitsheilung">Krankheitsheilung</option>
                              <option label="Kartografie">Kartografie</option>
                              <option label="Musik">Musik</option>
                              <option label="Schlösser öffnen">Schlösser öffnen</option>
                              <option label="Tätowieren">Tätowieren</option>
                              <option label="Töpfern">Töpfern</option>
                              <option label="Webkunst">Webkunst</option>
                              <option label="Bootsbau/Schiffbau">Bootsbau/Schiffbau</option>
                            <optgroup label="Nahkampfwaffen">
                              <option label="Dolche">Dolche</option>
                              <option label="Wuchtwaffen">Wuchtwaffen</option>
                              <option label="Raufen">Raufen</option>
                              <option label="Ringen">Ringen</option>
                              <option label="Säbel">Säbel</option>
                              <option label="Anderthalbhänder">Anderthalbhänder</option>
                              <option label="Fechtwaffen">Fechtwaffen</option>
                              <option label="Stangenwaffen">Stangenwaffen</option>
                              <option label="Kettenstäbe">Kettenstäbe</option>
                              <option label="Kettenwaffen">Kettenwaffen</option>
                              <option label="Schwerter">Schwerter</option>
                              <option label="Speere">Speere</option>
                              <option label="Stäbe">Stäbe</option>
                              <option label="Zweihandflegel">Zweihandflegel</option>
                              <option label="Zweihandwuchtwaffen">Zweihandwuchtwaffen</option>
                              <option label="Zweihandklingen">Zweihandklingen</option>
                              <option label="Pykmei">Pykmei</option>
                              <option label="Etarak">Etarak</option>
                            <optgroup label="Körperlich">
                              <option label="Wache halten">Wache halten</option>
                              <option label="Geländelauf">Geländelauf</option>
                              <option label="Springen">Springen</option>
                              <option label="Tauchen">Tauchen</option>
                              <option label="Athletik">Athletik</option>
                              <option label="Klettern">Klettern</option>
                              <option label="Körperbeherrschung">Körperbeherrschung</option>
                              <option label="Reiten">Reiten</option>
                              <option label="Schleichen">Schleichen</option>
                              <option label="Schwimmen">Schwimmen</option>
                              <option label="Selbstbeherrschung">Selbstbeherrschung</option>
                              <option label="Verstecken">Verstecken</option>
                              <option label="Singen">Singen</option>
                              <option label="Sinnenschärfe">Sinnenschärfe</option>
                              <option label="Tanzen">Tanzen</option>
                              <option label="Zechen">Zechen</option>
                              <option label="Akrobatik">Akrobatik</option>
                              <option label="Fliegen">Fliegen</option>
                              <option label="Gaukeleien">Gaukeleien</option>
                              <option label="Skifahren">Skifahren</option>
                              <option label="Stimmen Imitieren">Stimmen Imitieren</option>
                              <option label="Werfen">Werfen</option>
                            <optgroup label="Natur">
                              <option label="Umgang mit Tieren">Umgang mit Tieren</option>
                              <option label="Spurenlesen">Spurenlesen</option>
                              <option label="Wildnisleben">Wildnisleben</option>
                              <option label="Orientierung">Orientierung</option>
                              <option label="Ansitzjagd">Ansitzjagd</option>
                              <option label="Kräuter suchen">Kräuter suchen</option>
                              <option label="Nahrung sammeln">Nahrung sammeln</option>
                              <option label="Pirschjagd">Pirschjagd</option>
                              <option label="Fallenstellen">Fallenstellen</option>
                              <option label="Fesseln/Entfesseln">Fesseln/Entfesseln</option>
                              <option label="Fischen">Fischen</option>
                              <option label="Wettervorhersage">Wettervorhersage</option>
                              <option label="Tierheilkunde">Tierheilkunde</option>
                            <optgroup label="Schriften">
                              <option label="Altnordske Runen">Altnordske Runen</option>
                              <option label="Arcano">Arcano</option>
                              <option label="Altelfische Zeichen">Altelfische Zeichen</option>
                              <option label="Irrza">Irrza</option>
                              <option label="Sadhische Runen">Sadhische Runen</option>
                              <option label="Hiro-echuru">Hiro-echuru</option>
                              <option label="Nordahejmrunen">Nordahejmrunen</option>
                              <option label="Auretin">Auretin</option>
                              <option label="Elfische Zeichen">Elfische Zeichen</option>
                              <option label="Imperiale Zeichen">Imperiale Zeichen</option>
                              <option label="Glypho">Glypho</option>
                              <option label="Zwergenrunen">Zwergenrunen</option>
                              <option label="Inoda-echuru">Inoda-echuru</option>
                              <option label="Anbayjad">Anbayjad</option>
                              <option label="Bayjad">Bayjad</option>
                              <option label="Teiko-echuru">Teiko-echuru</option>
                              <option label="Thalassa B">Thalassa B</option>
                              <option label="Magiraglyph">Magiraglyph</option>
                              <option label="Naàhn Hieroglyphen">Naàhn Hieroglyphen</option>
                              <option label="Xeanhatl Hieroglyphen">Xeanhatl Hieroglyphen</option>
                              <option label="Thalsche Zeichen">Thalsche Zeichen</option>
                              <option label="Drak">Drak</option>
                              <option label="Tekka-echuru">Tekka-echuru</option>
                              <option label="Thalassa A">Thalassa A</option>
                            <optgroup label="Sprachen">
                              <option label="Altes Bajidai">Altes Bajidai</option>
                              <option label="Altes Zwergisch">Altes Zwergisch</option>
                              <option label="Altnordsk">Altnordsk</option>
                              <option label="Altra">Altra</option>
                              <option label="Aurento">Aurento</option>
                              <option label="Aurentum">Aurentum</option>
                              <option label="Avarun">Avarun</option>
                              <option label="Bajidai">Bajidai</option>
                              <option label="Charukom">Charukom</option>
                              <option label="Cron">Cron</option>
                              <option label="Darkanisch">Darkanisch</option>
                              <option label="Drachisch">Drachisch</option>
                              <option label="Elbisch">Elbisch</option>
                              <option label="Elfisch">Elfisch</option>
                              <option label="Eskalron">Eskalron</option>
                              <option label="Goblinisch">Goblinisch</option>
                              <option label="Gûi">Gûi</option>
                              <option label="Hiro">Hiro</option>
                              <option label="Hochelfisch">Hochelfisch</option>
                              <option label="Hochorkisch">Hochorkisch</option>
                              <option label="Imperial">Imperial</option>
                              <option label="Jotisch">Jotisch</option>
                              <option label="Koboldisch">Koboldisch</option>
                              <option label="Magira">Magira</option>
                              <option label="Mahud">Mahud</option>
                              <option label="Naàhn">Naàhn</option>
                              <option label="Nordahejmisch">Nordahejmisch</option>
                              <option label="Oda">Oda</option>
                              <option label="Orkisch">Orkisch</option>
                              <option label="Sadhisch">Sadhisch</option>
                              <option label="Signalo">Signalo</option>
                              <option label="Teiko">Teiko</option>
                              <option label="Tekka">Tekka</option>
                              <option label="Thalsch">Thalsch</option>
                              <option label="Trollsch">Trollsch</option>
                              <option label="Tryl">Tryl</option>
                              <option label="Urelfisch">Urelfisch</option>
                              <option label="Wolsch">Wolsch</option>
                              <option label="Xeanhatl">Xeanhatl</option>
                              <option label="ZheeZ">ZheeZ</option>
                              <option label="Zwergisch">Zwergisch</option>
                            <optgroup label="Wissen">
                              <option label="Theologie">Theologie</option>
                              <option label="Mathematik">Mathematik</option>
                              <option label="Legenden">Legenden</option>
                              <option label="Anatomie">Anatomie</option>
                              <option label="Architektur">Architektur</option>
                              <option label="Hüttenkunde">Hüttenkunde</option>
                              <option label="Spiele">Spiele</option>
                              <option label="Geografie">Geografie</option>
                              <option label="Historie">Historie</option>
                              <option label="Gesteinskunde">Gesteinskunde</option>
                              <option label="Heraldik">Heraldik</option>
                              <option label="Kriegskunst">Kriegskunst</option>
                              <option label="Kryptographie">Kryptographie</option>
                              <option label="Magietheorie">Magietheorie</option>
                              <option label="Mechanik">Mechanik</option>
                              <option label="Pflanzenkunde">Pflanzenkunde</option>
                              <option label="Philosophie">Philosophie</option>
                              <option label="Rechtskunde">Rechtskunde</option>
                              <option label="Wert schätzen">Wert schätzen</option>
                              <option label="Sprachenkunde">Sprachenkunde</option>
                              <option label="Politik">Politik</option>
                              <option label="Astronomie">Astronomie</option>
                              <option label="Tierkunde">Tierkunde</option>
                              <option label="Navigation">Navigation</option>
                              <option label="Okkultismus">Okkultismus</option>
                              <option label="Bibliothekskunde">Bibliothekskunde</option>
                              <option label="Archäologie">Archäologie</option>
                              <option label="Völkerkunde">Völkerkunde</option>
                              <option label="Bürokratie">Bürokratie</option>
                              <option label="Giftkunde">Giftkunde</option>
                            <optgroup label="reine AT-Kampftechniken">
                              <option label="Lanzenreiten">Lanzenreiten</option>
                              <option label="Peitsche">Peitsche</option>
                            <optgroup label="Gaben">
                              <option label="Magiegespür">Magiegespür</option>
                              <option label="Empathie">Empathie</option>
                              <option label="Geräuschhexerei">Geräuschhexerei</option>
                              <option label="Kräfteschub">Kräfteschub</option>
                              <option label="Talentschub">Talentschub</option>
                              <option label="Tierempathie">Tierempathie</option>
                              <option label="Gefahreninstinkt">Gefahreninstinkt</option>
                              <option label="Zwergennase">Zwergennase</option>
                              <option label="Prophezeien">Prophezeien</option>
                            <optgroup label="Meta">
                              <option label="Kampf">Kampf</option>
                              <option label="Fernkampf">Fernkampf</option>
                              <option label="Medizin">Medizin</option>
                              <option label="Lesen/Schreiben">Lesen/Schreiben</option>
                          </select>
                          
                          +/-<input type="text" name="mod_'.$position.'" value="0">
';



    echo '<label>Fertigkeit:
            '.$talent_select.'
            </label>';

    echo '<h2>Probe bestanden</h2>';
    input_text('win_'.$position);
    input_loot('win_'.$position);

    echo '<h2>Probe misslungen</h2>';
    input_text('loss_'.$position);
    input_loot('loss_'.$position);
}

/**
 * simple challenge with ep/cash/win
 * @param $position - start, win - Zweig bei Verschachtelung, loss - Zweig bei Verschachtelung
 * @param $type - simple, win, loss
 * @return string
 */
function aoqml_challenge($position, $type){
  $content = '';
  $content .= "\n";
  $content .= htmlspecialchars('<challenge talent="'.$_POST['talent_'.$position].'" mod="'.$_POST['mod_'.$position].'">') . "\n";

  // success
  $content .= "\n" . '  ' . htmlspecialchars('<success>') . "\n";

  $content .= '    ' . htmlspecialchars($_POST['win_'.$position]) . "\n";

  if(intval($_POST['ware_id_win_'.$position]) > 0){
    $content .= '    ' . htmlspecialchars('<take item="#'.$_POST['ware_id_win_'.$position].'"/>') .
      htmlspecialchars('<!-- '.$_POST['ware_comment_win_'.$position].' -->') .
      "\n";
  }

  if(intval($_POST['ep_win_'.$position]) > 0){
    $content .= '    ' . htmlspecialchars('<set attribute="EP" inc="'.$_POST['ep_win_'.$position].'"/>') . "\n";
  }

  if(intval($_POST['cash_win_'.$position]) > 0){
    $content .= '    ' . htmlspecialchars('<set attribute="cash" inc="'.$_POST['cash_win_'.$position].'"/>') . "\n";
  }


  if($type == 'win'){
    $content .= aoqml_challenge('win', 'simple');
  }


  $content .= '  ' . htmlspecialchars('</success>') . "\n\n";


  // failure
  $content .= "\n" . '  ' . htmlspecialchars('<failure>') . "\n";
  $content .= '    ' . htmlspecialchars($_POST['loss_'.$position]) . "\n";

  if(intval($_POST['ware_id_loss_'.$position]) > 0){
    $content .= '    ' . htmlspecialchars('<take item="#'.$_POST['ware_id_loss_'.$position].'"/>') .
      htmlspecialchars('<!-- '.$_POST['ware_comment_loss_'.$position].' -->') .
      "\n";
  }

  if(intval($_POST['ep_loss_'.$position]) > 0){
    $content .= '    ' . htmlspecialchars('<set attribute="EP" inc="'.$_POST['ep_loss_'.$position].'"/>') . "\n";
  }

  if(intval($_POST['cash_loss_'.$position]) > 0){
    $content .= '    ' . htmlspecialchars('<set attribute="cash" inc="'.$_POST['cash_loss_'.$position].'"/>') . "\n";
  }

  $content .= '  ' . htmlspecialchars('</failure>') . "\n\n";

  $content .= htmlspecialchars('</challenge>') . "\n";

  return $content;
}