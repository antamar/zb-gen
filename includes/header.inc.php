<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
<head>
    <title>Antamar - Zufallsbegegnungseditor</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta name="Keywords" content="Antamar, Zufallsbegegnungen" />

    <script src="assets/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({  selector:'textarea',
                            entity_encoding: "raw",
                            selector: "textarea:not(.notiny)"});</script>

    <style type="text/css">
        .icon {
            border-radius: 10px;
            box-shadow: 0px 0px 5px 5px #7e6969;
            margin: 6px;
        }

        figure {
            width: 30%;
            text-align: center;
            font-style: italic;
            font-size: smaller;
            text-indent: 0;
            border: thin silver solid;
            margin: 0.5em;
            padding: 0.5em;
        }
    </style>

</head>
<body>
<h1>Antamar Zufallsbegegnungseditor</h1>
<p><a href="/">Startseite - Eingaben verwerfen</a></p>