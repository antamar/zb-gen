<?php

/**
 * Get flow id and include matching template with input forum
 * or include with $aoqml=true to generate posted content
 */

require_once(dirname(__FILE__) . '/includes/header.inc.php');
require_once(dirname(__FILE__) . '/includes/snippets/text.php');
require_once(dirname(__FILE__) . '/includes/snippets/challenge.php');
require_once(dirname(__FILE__) . '/includes/snippets/loot.php');



// get flow id and include matching template
if(isset($_GET['flow'])){
    $flow_id = intval($_GET['flow']); ?>
    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
    <?php
        require_once(dirname(__FILE__) . '/includes/flows/'.$flow_id.'.php'); ?>


        <p>
            Dein Name: <input type="text" name="autor" size="20">
            ZB-Titel: <input type="text" name="zb_name" size="50">

            <input type="hidden" name="flow" value="<?=$flow_id;?>">
            <input type="submit" name="send" value="Generieren">
        </p>
    </form>
<?php
}
elseif (isset($_POST['flow'])){ // dont include forms only generate aoqml
    $flow_id = intval($_POST['flow']);
    $aoqml = true;

    require_once(dirname(__FILE__) . '/includes/flows/'.$flow_id.'.php');
}


require_once(dirname(__FILE__) . '/includes/footer.inc.php');