<?php
require_once(dirname(__FILE__) . '/includes/header.inc.php');
?>

<h2>Wie funktioniert das hier?</h2>
<ul>
    <li>Wähle einen fertigen Ablauf.</li>
    <li>Fülle die Textfelder aus.</li>
    <li>Wähle die passende Probe oder den passenden Gegner.</li>
    <li>Wähle die Belohnung.</li>
    <li>Generiere deine fertige Zufallsbegegnung.</li>
    <li>Lade sie in das Antamar-Wiki zur Kontrolle und zum Einbau.</li>
    <li>Schaue später im Antamar-Wiki vorbei ob es Anmerkungen gab.</li>
</ul>

<h2>Zufallsbegegnungs-Templates</h2>

    <h3>Ablauf 1</h3>
    <figure>
        <a href="template.php?flow=1"><img src="assets/flow1.png" class="icon"></a>
        <figcaption>Einstiegstext → Probe → Gelungen/Misslungen</figcaption>
    </figure>

    <h3>Ablauf 2</h3>
    <figure>
        <a href="template.php?flow=2"><img src="assets/flow2.png" class="icon"></a>
        <figcaption>Einstiegstext → Probe → Gelungen/Misslungen → Probe → Gelungen/Misslungen</figcaption>
    </figure>

<?php
require_once(dirname(__FILE__) . '/includes/footer.inc.php');